/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../MuonMDT_CablingAlg.h"
#include "../TwinTubeMappingCondAlg.h"

DECLARE_COMPONENT(MuonMDT_CablingAlg)
DECLARE_COMPONENT(Muon::TwinTubeMappingCondAlg)
